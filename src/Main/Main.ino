
#include "Led.h"
#include "Button.h"
#include <ProoveSignal.h>
#include "RF.h"

#include "DHT22_Temp.h"



/* -------- TODO --------
 * RF-FIX: Endre navn til .cpp, endre char til int. lag egen .zip fil fra mac'en
 * 
 * Add temp/moisture sensor: DHT22 https://create.arduino.cc/projecthub/MisterBotBreak/how-to-use-temperature-and-humidity-dht-sensors-9e5975 , https://www.theengineeringprojects.com/wp-content/uploads/2019/02/intro-to-dht22.jpg
 * Add RF: #include <ProoveSignal.h> http://tech.jolowe.se/home-automation-rf-protocols/ , https://rayshobby.net/interface-with-remote-power-sockets-final-version/ 
 * 
 * 
 * 
 * State machine:
 *    -Setup: buttonpress, wps, nettside, kanskje godt nok med ein loop i void setup()
 *    -fetch api, nettside, read temp sensor, switch rele, control temperature etc
 * 
 * 
 */

#define LED_1_PIN 5         // D1 = GPIO5
#define BUTTON_PIN 4        // D2 = GPIO4
#define TRANSMISSION_PIN 14 // D5 = GPIO14
//#define TEMP_PIN 13         // D7 = GPIO13


Led led1(LED_1_PIN);
Led led2(LED_BUILTIN);

Button button1(BUTTON_PIN);

RFtransmitter rfTransmitter(TRANSMISSION_PIN);



//Toggle button test 1
int state = HIGH;         // the current state of the output pin
int reading;              // the current reading from the input pin
int previous = LOW;       // the previous reading from the input pin
long lastToggleTime = 0;  // the last time the output pin was toggled
long debounce = 200; 



void setup() { 
  // put your setup code here, to run once:
  Serial.begin(9600);
  Serial.println("Hello world :]");

  //dht.begin();
  initTempSensor();
}



void loop() {


//Toggle button test 2.
/*
if (button1.isToggled()) {
   if (button1.getToggleState() == HIGH){
    rfTransmitter.socket(HIGH);
    led2.on();
    }else{
      rfTransmitter.socket(LOW);
      led2.off();
      }
}
*/

  Serial.print("Temperature: "); 
  Serial.println(readTemperature());
  //Serial.println(" *C ");
  // temp timer og pid timer. Pid kaller temp --> 1 timer
  
  //button timer?- naj



//Toggle button test 1
  reading = digitalRead(BUTTON_PIN);
  // If the input just went from LOW and HIGH and we've waited long enough to ignore any noise on the circuit, toggle the output pin and remember the time
  if (reading == HIGH && previous == LOW && millis() - lastToggleTime > debounce) {
    
    lastToggleTime = millis();    
    if (state == HIGH){
      state = LOW;
      rfTransmitter.socket(HIGH);
      Serial.println("state=LOW, RF ON");
    }
    else{
      state = HIGH;
      rfTransmitter.socket(LOW); 
      Serial.println("state=high, RF OFF");
    }
  }
  //digitalWrite(LED_BUILTIN, state);
  digitalWrite(LED_1_PIN, state);
  previous = reading;
  
}
