#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <ArduinoJson.h>

// WiFi Parameters
const char* ssid = "Get-2G-DD0F81";
const char* password = "HFXFCHFNUL";

void setup() {
  Serial.begin(115200);
  WiFi.begin(ssid, password);
 
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Connecting...");
  }
}

void loop() {
  // Check WiFi Status
  if (WiFi.status() == WL_CONNECTED) {
    HTTPClient http;  //Object of class HTTPClient
    //http.begin("http://jsonplaceholder.typicode.com/users/1");
    http.begin("http://www.nordpoolgroup.com/api/marketdata/page/10?currency=NOK&endDate=27-04-2021");
    int httpCode = http.GET();
    Serial.println(httpCode);
    //Check the returning code                                                                  
    if (httpCode > 0) {
      // Parsing
      
      // The filter: it contains "true" for each value we want to keep
      StaticJsonDocument<600> filter;
      //DynamicJsonDocument filter;
      //filter["data"]["Columns"][0]["Index"] = true;
      filter["data"]["Rows"][0]["Columns"][0]["Value"] = true;
      //filter["Index"] = true;
      //filter["name"] = true;

      // Deserialize the document
      //StaticJsonDocument<6000> doc;
      DynamicJsonDocument doc(1024);
      
      //DeserializationError error = deserializeJson(doc, http.getString(), DeserializationOption::Filter(filter));
      Serial.println(http.getString());
      DeserializationError error = deserializeJson(doc, http.getString());
   
   // Test if parsing succeeds.
  if (error) {
    Serial.print(F("deserializeJson() failed: "));
    Serial.println(error.f_str());
  }

  // Print the result
  serializeJsonPretty(doc, Serial);
    
    }
    http.end();   //Close connection
  }
  // Delay
  delay(60000);
}
