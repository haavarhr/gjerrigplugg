#include <ArduinoJson.h>
#include <ESP8266WiFi.h>
#include <ESPAsyncWebServer.h>
#include "DHT.h"
#include <ESPAsyncTCP.h>

// hent dato/klokkeslett
#include <NTPClient.h> 
#include <WiFiUdp.h>

// rf transmitter
#include "RF.h"
#include "Button.h"

//
#include "wps.hpp"
//#include "preso.hpp"


// Data polling interval in hours. Leave (36e5) untouched. Adjust the left factor to adjust hours..
const long pollInterval = 6e4; //36e5;

// Time to wait before rerying connection if no WiFi connection is present.
const int noNetRetryDelay = 1e3;
unsigned long startMillis;
unsigned long currentMillis;

// Used for WiFi busy waiting.
int counter = 0;



// KILDE: https://roboticadiy.com/esp8266-iot-live-sensor-data-plotter-to-web-page/


#define LED_1_PIN 5         // D1 = GPIO5
#define BUTTON_PIN 4        // D2 = GPIO4
#define TRANSMISSION_PIN 14 // D5 = GPIO14
//#define TEMP_PIN 13         // D7 = GPIO13


//Led led1(LED_1_PIN);
//Led led2(LED_BUILTIN);

Button button1(BUTTON_PIN);

RFtransmitter rfTransmitter(TRANSMISSION_PIN);


// Replace with your network credentials
const char* ssid = "Get-2G-DD0F81";
const char* password = "HFXFCHFNUL";


const long utcOffsetInSeconds = 7200; //+2 timer

char daysOfTheWeek[7][12] = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};

// Define NTP Client to get time
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "pool.ntp.org", utcOffsetInSeconds);


//desse kan hentes fra Nordpool API
//24 rows 2 columns 
//float strompris[24][2] = { { x, y }, { x, y }, { x, y }, { x, y }, { x, y }, { x, y }, { x, y }, { x, y }, { x, y }, { x, y }, { x, y }, { x, y }, { x, y }, { x, y }, { x, y }, { x, y }, { x, y }, { x, y }, { x, y }, { x, y }, { x, y }, { x, y }, { x, y }, { x, y } }; 
//nvm hehe, 24 columns, index= hour
float strompris[24] = {47.173, 46.501, 47.093, 47.945, 49.007, 52.275, 69.577, 81.676, 85.775, 70.158, 54.601, 51.614, 49.669, 49.569, 50.962, 54.430, 56.094, 63.763, 65.356, 73.857, 63.462, 60.806, 55.102, 49.108};
float averagePris = 58.149;
float minPris = 46.501;
float maxPris = 85.775;


/* 27.04.2021
00 - 01 471,73
01 - 02 465,01
02 - 03 470,93
03 - 04 479,45
04 - 05 490,07
05 - 06 522,75
06 - 07 695,77
07 - 08 816,76
08 - 09 857,75
09 - 10 701,58
10 - 11 546,01
11 - 12 516,14
12 - 13 496,69
13 - 14 495,69
14 - 15 509,62
15 - 16 544,30
16 - 17 560,94
17 - 18 637,63
18 - 19 653,56
19 - 20 738,57
20 - 21 634,62
21 - 22 608,06
22 - 23 551,02
23 - 00 491,08
*/

// 47.173, 46.501, 47.093, 47.945, 49.007, 52.275, 69.577, 81.676, 85.775, 70.158, 54.601, 51.614, 49.669, 49.569, 50.962, 54.430, 56.094, 63.763, 65.356, 73.857, 63.462, 60.806, 55.102, 49.108


//TODO: lag temp referanse. hysteris control / bang-bang control


// normalize with min/max values, invert, lift average to 20 degrees

//map(value, fromLow, fromHigh, toLow, toHigh) 
//builtin arduino map takler ikkje floats.. modda for å takle floats under

float mapfloat(float x, float in_min, float in_max, float out_min, float out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

//float tmpArray[24] = strompris;
float tempRef[24]; // = strompris; 
float tempLB = 15.0;
float tempUB = 28.0;

String stringTempRef = "";

// "regulering"
float hysteresisBand = 0.6; // største tillatte abs avvik fra referanse
bool ovenState = true;



#define DHTPIN 13 // D7 = GPIO13 connected to the DHT sensor

// Uncomment the type of sensor in use:
//#define DHTTYPE DHT11 // DHT 11
#define DHTTYPE DHT22 // DHT 22 (AM2302)
//#define DHTTYPE DHT21 // DHT 21 (AM2301)

DHT dht(DHTPIN, DHTTYPE);

// current temperature & humidity, this will be updated in loop function
float t = 0.0;
float tf = 0.0;
float h = 0.0;

// Create AsyncWebServer object on port 80
AsyncWebServer server(80);

unsigned long previousMillis = 0; //stoe last time DHT was updated
const long interval = 1000*10; // Updates DHT readings every 60 seconds

//web page
const char index_html[] PROGMEM = R"webpage(
<!DOCTYPE HTML><html>
<head>
<meta name="viewport" charset="UTF-8" content="width=device-width, initial-scale=1">
<script src="https://code.highcharts.com/8.0/highcharts.js"></script>
<style>
body {
min-width: 300px;
max-width: 800px;
height: 400px;
margin: 0 auto;
}
h2 {
font-family: Arial;
font-size: 2.5rem;
text-align: center;
}
</style>
</head>
<body>
<h2>Gjerrigplugg</h2>
<div id="temperature-chart" class="container"></div>
<div id="fahrenheit-chart" class="container"></div>
<div id="humidity-chart" class="container"></div>
</body>
<script>

// henter dagens dato javascript
const timestamp1970 = (new Date()).getTime();
let today = new Date(timestamp1970);
//alert(today); denne stemmer med tidssona! Tue Apr 27 2021 11:45:14 GMT+0200 (Central European Summer Time)


var chartT = new Highcharts.Chart({
chart:{ renderTo : 'temperature-chart' },
title: { text: 'Temperatur' },

series: [{
  id: 'series-1',
  name: 'Målt Temperatur',
  showInLegend: true, //Var false, viser navn på kurva under grafen
  data: [],
  color: '#059e8a'
  },
  {
  id: 'series-2',
  name: 'Min temperatur ref', // min temp: 20. max temp: 28. gang med minus 1, legg til average? loop og endre for høge/lave til min/max verdien. alternativt: venstreforskyv kurva
  showInLegend: true, //Var false, viser navn på kurva under grafen
  //data: [], // kanskje fylle inn tulleverdi så grafen ikkje e tom ved oppstart? :]
  
  data:[], //[[today.setHours(0+2), 20.923], [today.setHours(1+2), 20.771], [today.setHours(2+2), 20.842], [today.setHours(3+2), 20.994], [today.setHours(4+2), 23.481],  [today.setHours(21), 22.369], [ today.setHours(22), 21.975], [today.setHours(23+2), 21.580]],
  dashStyle: 'dash',
  color: 'black' 
  } 
],

plotOptions: {
line: { animation: false,
        dataLabels: { enabled: true }
  },
},

xAxis: { type: 'datetime',
dateTimeLabelFormats: { second: '%H:%M:%S' }
},
yAxis: {
title: { text: 'Temperatur (Celsius)' }
},
credits: { enabled: false }
});

// ---------- end highchart temp -------------

var updateIntervalT = 60;

setInterval(function ( ) {
var xhttp = new XMLHttpRequest();
xhttp.onreadystatechange = function() {
if (this.readyState == 4 && this.status == 200) {
var x = (new Date()).getTime() + 7200000, //+ 7200000 = 2 timer i millisekund pga tidssone
//const timestampX = (new Date()).getTime();
//let x = new Date(timestampX); // Tue Apr 27 2021 11:51:17 GMT+0200 (Central European Summer Time)
//alert(x);
//x2 = x;
y = parseFloat(this.responseText);
//console.log(x); //1619512195923
if(chartT.series[0].data.length > 1440) {  // 1440 = antall datapunkt før grafen scroller. måler temp 60 ganger i timen(kvart 1min) 60*24=1440
chartT.series[0].addPoint([x, y], true, true, true);
//chartT.series[1].addPoint([x2, y2], true, true, true); // forhåndsberegna, 24 timer frem i tid
} else {
chartT.series[0].addPoint([x, y], true, false, true);

//chartT.series[1].addPoint([x2, y2], true, false, true);
}
//updateIntervalT = 60; funke ikkje, bruk setTimeout
}
};
xhttp.open("GET", "/temperature", true);
xhttp.send();
}, 1000*updateIntervalT  ) ; // hvor ofte datapunkt skal legges til i grafen, kvart 1. minutt. prob: går 1min før første punkt







// ---------------- strøm highchart ------------------------
var chartF = new Highcharts.Chart({
chart:{ renderTo:'fahrenheit-chart'},
title: { text: 'Strømpris i øre/kWh' },

series: [{
  id: 'series-3',
  name: 'pris',  
  showInLegend: true, //pris henta 12.04.2021, Average 23.365
  //data: [[0, 21.054], [1, 20.923], [2, 20.771], [3, 20.842], [4, 20.994], [5, 23.481], [6, 27.243], [7, 27.486], [8, 28.336], [9, 25.180], [10, 25.201], [11, 24.837], [12, 24.695], [13, 23.754], [14, 23.431], [15, 23.411], [16, 23.239], [17, 23.077], [18, 22.116], [19, 22.238], [20, 22.531], [21, 22.369], [22, 21.975], [23, 21.580]], 
  data: [[today.setHours(0+2,0,0), 47.173], [today.setHours(1+2), 46.501], [today.setHours(2+2),  47.093], [today.setHours(3+2), 47.945], [today.setHours(4+2), 49.007], [today.setHours(5+2),  52.275],[today.setHours(6+2), 69.577],[today.setHours(7+2),  81.676],[today.setHours(8+2), 85.775],[today.setHours(9+2), 70.158],[today.setHours(10+2), 54.601],[today.setHours(11+2), 51.614],[today.setHours(12+2), 49.669],[today.setHours(13+2), 49.569],[today.setHours(14+2),50.962],[today.setHours(15+2), 54.430],[today.setHours(16+2), 56.094],[today.setHours(17+2), 63.763],[today.setHours(18+2), 65.356],[today.setHours(19+2), 73.857],[today.setHours(20+2), 63.462],[today.setHours(21+2), 60.806], [ today.setHours(22+2), 55.102], [today.setHours(0+1), 49.108]],
  // 47.173, 46.501, 47.093, 47.945, 49.007, 52.275, 69.577, 81.676, 85.775, 70.158, 54.601, 51.614, 49.669, 49.569, 50.962, 54.430, 56.094, 63.763, 65.356, 73.857, 63.462, 60.806, 55.102, 49.108



  
  zoneAxis: 'x',
  zones: [{value: 0}, {dashStyle: 'dot'}] // 0=datetime hour, begynner med heile grafen stipla, blir oppdatert
  
  }],
plotOptions: {
  line: { animation: false, dataLabels: { enabled: true }} 
},

xAxis: {
type: 'datetime',
dateTimeLabelFormats: {second: '%H:%M:%S'}
},
yAxis: {
title: { text: 'øre/kWh' }
},
credits: { enabled: false }
});

var runOnce = 1; // jankyboooiii henter tempref kurva 1 gang

setInterval(function ( ) {
var xhttp = new XMLHttpRequest();
xhttp.onreadystatechange = function() {
if (this.readyState == 4 && this.status == 200) {
  //var x = (new Date()).getTime(), y = parseFloat(this.responseText);

  var tempRefArr = this.responseText.split(',');
  //console.log(tempRefArr); //["16.82", "17.04", "16.84", "24.06", "23.71", "22.63", "16.90", "15.00", "15.00", "16.71", "21.86", "22.85", "23.49", "23.52", "23.06", "21.91", "21.36", "18.82", "18.30", "15.48", "18.92", "19.80", "15.00", "16.18"]

  // kjører 1 gang

  
  
  if (runOnce == 1){
      var i;
      for (i = 0; i < tempRefArr.length; i++) {
      y = parseFloat(tempRefArr[i]);
      const timestamp234 = (new Date()).getTime();
      let today234 = new Date(timestamp234);
      let x = today234.setHours(i+2, 0,0);
      //console.log(x);
      //x2 = x.setDate(28);
      chartT.series[1].addPoint([x, y], true, false, true);
      }
      runOnce = 0;
      console.log("added tempRef!");
    }


   // chartT.series[1].update({
   // data: series.yData tempRefArr
    //});

  

  //console.log("hallo?"); //funker!
  //console.log(x.getHours());
  //x.getHours(); 

  const timestamp2 = (new Date()).getTime();
  let temp22 = new Date(timestamp2);
  const hour22 = temp22.getHours();
  const now2 = temp22.setHours(hour22 + 2);


  //flytter på den stipla fremtidsprisen etterkvart som tida går today.setHours(21+1)
  chartF.series[0].update({
    zones: [{value: now2}, {dashStyle: 'dot'}] //(new Date()).getHours()
    });
    
  
  
  //console.log(Date.now()); // funker! resultat: 1619420125457
  //alert("hallo"); //funker!

//console.log(this.responseText);
if(chartF.series[0].data.length > 1440) {
//chartF.series[0].addPoint([x, y], true, true, true);
} else {
//chartF.series[0].addPoint([x, y], true, false, true);
}
}
};
xhttp.open("GET", "/fahrenheit", true);
xhttp.send();
}, 1000) ; // todo: bytt ut med 1000*x endrer x fra 1 til 60*60 etter første kjøring!



// -------------- Start humidity ----------------
// TODO: vis om ovnen er av eller på

var chartH = new Highcharts.Chart({
chart:{ renderTo:'humidity-chart' },
title: { text: 'Humidity (%)' },
series: [{
showInLegend: false,
data: []
}],
plotOptions: {
line: { animation: false,
dataLabels: { enabled: true }
},
series: { color: '#18009c' }
},
xAxis: {
type: 'datetime',
dateTimeLabelFormats: { second: '%H:%M:%S' }
},
yAxis: {
title: { text: 'Humidity (%)' }
},
credits: { enabled: false }
});
setInterval(function ( ) {
var xhttp = new XMLHttpRequest();
xhttp.onreadystatechange = function() {
if (this.readyState == 4 && this.status == 200) {
var x = (new Date()).getTime(),
y = parseFloat(this.responseText);
//console.log(this.responseText);
if(chartH.series[0].data.length > 50) {
chartH.series[0].addPoint([x, y], true, true, true);
} else {
chartH.series[0].addPoint([x, y], true, false, true);
}
}
};
xhttp.open("GET", "/humidity", true);
xhttp.send();
}, 1000*60 ) ;

// ---------------- END HUMIDITY --------------------

</script>
</html>)webpage";



// ---------------- VOID SETUP ----------------------

void setup(){
// Serial port for debugging purposes
Serial.begin(115200);

pinMode(LED_1_PIN, OUTPUT);



//halvor fake wps button,  blink led
while(!button1.isPressed()){
  ESP.wdtFeed();
  }
for(int i = 0; i < 5; ++i) {
  digitalWrite(LED_1_PIN, !digitalRead(LED_1_PIN));
  delay(500);
}



// før wps blei forsøkt implementert
// Connect to Wi-Fi
WiFi.begin(ssid, password);
Serial.println("Connecting to WiFi");
while (WiFi.status() != WL_CONNECTED) {
delay(1000);
Serial.println(".");
}

// Print ESP32 Local IP Address
Serial.println(WiFi.localIP());


//wps
  /*delay(1000);
  Serial.println("Gjerrigplugg");
  startMillis = millis();
  if(WiFi.SSID().length() > 0){ 
    Serial.print("Connecting to network: ");
    Serial.println(WiFi.SSID());
    WiFi.disconnect(true);
    WiFi.mode(WIFI_STA);
    WiFi.begin(WiFi.SSID());
    while (WiFi.status() != WL_CONNECTED) {
      delay(500);
      Serial.print(".");
    }
    Serial.println("");
    Serial.println("WiFi connected");
    Serial.println("IP address set: ");
    Serial.println(WiFi.localIP()); // Print LAN IP
  }
  attachInterrupt(digitalPinToInterrupt(pinWps), btnWpsPress, FALLING);

  while (WiFi.status() != WL_CONNECTED) { //during lost connection, print dots
    delay(2e3);
    Serial.print(".");
  }
*/
  // TODO: FOKIN HACK IT, PRINT DET SOM BLIR PRINTA OG SÅ KOBLE TIL MED SSID OG PASSORD SOM VANLIG, skru ned wps timer til 10 sec.  




// ---------------- TEST ARDUINOJSON ---------------
/*
  const char* input_json = "http://www.nordpoolgroup.com/api/marketdata/page/10?currency=NOK&endDate=27-04-2021";
  //const __FlashStringHelper* input_json = "http://www.nordpoolgroup.com/api/marketdata/page/10?currency=NOK&endDate=27-04-2021";
  //const __FlashStringHelper* input_json =
  
  // The filter: it contains "true" for each value we want to keep
  StaticJsonDocument<600> filter;
  //DynamicJsonDocument filter;
  //filter["data"]["Columns"][0]["Index"] = true;
  //filter["data"]["Rows"][0]["Columns"][0]["Value"] = true;
  filter["Index"] = true;

  // Deserialize the document
  //StaticJsonDocument<6000> doc;
  DynamicJsonDocument doc(1024);
  
  Serial.println(input_json);
  
  //DeserializationError error = deserializeJson(doc, input_json, DeserializationOption::Filter(filter));
  DeserializationError error = deserializeJson(doc, input_json);
   
   // Test if parsing succeeds.
  if (error) {
    Serial.print(F("deserializeJson() failed: "));
    Serial.println(error.f_str());
  }

  // Print the result
  serializeJsonPretty(doc, Serial);
  */
// -------------- end arduino json ---------------

//String stringTempRef = "";
//stringTempRef[24][5];
char bufferString[5];

//Lag temperaturreferanse kurva: map, invert, lift by tempLB
for(int i = 0; i < 24; ++i) {
      tempRef[i] = mapfloat( strompris[i], minPris, maxPris, tempLB, tempUB);
      tempRef[i] = (tempRef[i]*(-1))+ tempUB + tempLB/1.3;
      
        // justerer nattetemperaturen litt ned mellom kl 22 og kl 03, kunne vert brukersatt variabel
      if(i <= 2 || i >= 22){
        tempRef[i] = tempRef[i] -tempLB/2;
        }

      if (tempRef[i] < tempLB){
        tempRef[i] = tempLB;
        }

        // strengkonvertering
       dtostrf(tempRef[i],5, 2, bufferString); //6 = 1 padda space!
       stringTempRef += bufferString;
       if (i != 23){
       stringTempRef += ",";
       }
        
      Serial.print(tempRef[i]);
      Serial.println(""); // start new line of output
}; 
Serial.println(stringTempRef); 





// Route for root / web page
server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
request->send_P(200, "text/html", index_html);
});
server.on("/temperature", HTTP_GET, [](AsyncWebServerRequest *request){
request->send_P(200, "text/plain", String(t).c_str()); // sleng inn dette: String(FREMTIDIG_TEMP).c_str() Lag ein string og splitt i ajax med: var floats = input.split(' ').map(parseFloat)
});
server.on("/fahrenheit", HTTP_GET, [](AsyncWebServerRequest *request){
request->send_P(200, "text/plain", String(stringTempRef).c_str()); //String(tempRef[1]).c_str() <-- funker //String(tempRef).c_str() // error: no matching function for call to 'AsyncWebServerRequest::send_P(int, const char [11], float [24])'
});
server.on("/humidity", HTTP_GET, [](AsyncWebServerRequest *request){
request->send_P(200, "text/plain", String(h).c_str());
});

// Start server
server.begin();
timeClient.begin(); // NTP time
dht.begin();

}




// ---------------- VOID LOOP ----------------------

void loop(){ 

  // wps, funke ikkje, og er eit problem med server.begin over...
  /*
  if (WiFi.status() == WL_CONNECTED) { //if we are connected to eduroam network
    counter = 0; //reset counter
  } else if (WiFi.status() != WL_CONNECTED && WiFi.SSID().length()) { //if we lost connection, retry
    WiFi.begin(WiFi.SSID());
  }
  while (WiFi.status() != WL_CONNECTED) { //during lost connection, print dots
    delay(2e3);
    Serial.print(".");
  }
  */


  

// NOK/MWh * 1/100 = øre/KWh
  
  unsigned long currentMillis = millis();
  if (currentMillis - previousMillis >= interval) {
    timeClient.update(); // NTP time
    Serial.print(daysOfTheWeek[timeClient.getDay()]);
    Serial.print(", ");
    Serial.print(timeClient.getHours());
    Serial.print(":");
    Serial.print(timeClient.getMinutes());
    Serial.print(":");
    Serial.println(timeClient.getSeconds());

    //Serial.println(WiFi.localIP());

    
    // save the last time you updated the DHT values
    previousMillis = currentMillis;
    // Read temperature as Celsius (the default)
    float currentT = dht.readTemperature();
    
    // if temperature read failed, we don't want to change t value
    if (isnan(currentT)) {
      Serial.println("Failed to read from DHT sensor!");
      Serial.println(currentT);
    }
    else {
      t = currentT - 1.8; // trekk fra 1.8 grader for å matche vintemp sensor
      Serial.println(t);

      float tempError = t - tempRef[timeClient.getHours()];
      Serial.print("tempError: ");
      Serial.println(tempError);

      if (abs(tempError) >= hysteresisBand){
        
        if (tempError > 0 && ovenState == true){
            rfTransmitter.socket(LOW);
            ovenState = false;
            digitalWrite(LED_1_PIN, LOW); 
            Serial.println("Oven off");
            
         }
        else if (tempError < 0 && ovenState == false) {
            rfTransmitter.socket(HIGH);
            ovenState = true;
            digitalWrite(LED_1_PIN, HIGH);
            Serial.println("Oven on");
            
         }
       
       }
       /* 
      if (tempError >= hysteresisBand) && (ovenState == true) {
      //sendRF.off();
      OvenState = false;
      }
      else if (Ovenstate == false) {
      //sendRF.on();
      OvenState = true;
      } else {
      //Serial.Println("happy");
      }*/

      
    }
    // Read temperature as Fahrenheit. Get price from array
    float currentTf = dht.readTemperature(true);
    // if temperature read failed, we don't want to change tf value
    if (isnan(currentTf)) {
      Serial.println("Failed to read from DHT sensor!");
    }
    else {
      //tf = currentTf;
      //Serial.println(tf);
    }
    
    
    // Read Humidity
    float currentH = dht.readHumidity();
    // if humidity read failed, we don't want to change h value 
    if (isnan(currentH)) {
      Serial.println("Failed to read from DHT sensor!");
    }
    else {
      h = currentH;
      Serial.println(h);
    }
  }
}
