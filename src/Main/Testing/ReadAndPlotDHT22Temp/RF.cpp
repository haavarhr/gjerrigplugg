#include "RF.h"


RFtransmitter::RFtransmitter(char transmissionPin) {
  this->transmissionPin = transmissionPin;
  init();
}

// kjøres automatisk når du lager objektet
void RFtransmitter::init() {
  
  // Set pin to output
  pinMode(transmissionPin, OUTPUT);
  // Set the remote id
  signal.parts.remote = 0x1683805;
  //Pick socket to control 
  signal.parts.unit = 10;  // plugg 1= "00" 2= "01" 3="10" alle="11"
  // Turn off group sending
  signal.parts.group = PROOVE_SIGNAL_OFF;
  
  // Turn the power outlet off
  //signal.parts.status = PROOVE_SIGNAL_OFF;
  //prooveSignalSendData(transmissionPin, signal.data);

}

void RFtransmitter::socket(byte powerState){
  //signal.parts.unit = 10; 
  if (powerState == HIGH){
    signal.parts.status = PROOVE_SIGNAL_ON;
    }else if (powerState ==  LOW){
      signal.parts.status = PROOVE_SIGNAL_OFF;
      }else{
        Serial.println("Error: Wrong input to RFtransmitter.socket(byte state)");
        }
  prooveSignalSendData(transmissionPin, signal.data);
  };
/*
void RFtransmitter::socketOFF(){
  //signal.parts.unit = 10; 
  signal.parts.status = PROOVE_SIGNAL_OFF;
  prooveSignalSendData(transmissionPin, signal.data);
  };
*/




// ------- OLD --------
/*
ProoveSignal signal;
char transmissionPin = 14; //D5 = GPIO14

// TODO:, argument for remote ID,transmissionPin, og signal.parts.unit,  Objekt? nai
void RF_init() {
  
  // Set pin to output
  pinMode(transmissionPin, OUTPUT);

  // Set the remote id
  signal.parts.remote = 0x1683805;

  //Pick socket to control 
  signal.parts.unit = 10;  // plugg 1= "00" 2= "01" 3="10" alle="11"

  // Turn off the group
  signal.parts.group = PROOVE_SIGNAL_OFF;

  // Turn the power outlet off
  //signal.parts.status = PROOVE_SIGNAL_OFF;
  //prooveSignalSendData(transmissionPin, signal.data);
};


void sendRF_ON(){
  //RF_init();
  //Pick socket to control 
  signal.parts.unit = 10; 
  signal.parts.status = PROOVE_SIGNAL_ON;
  //Serial.println(signal.parts.unit); 
  prooveSignalSendData(transmissionPin, signal.data);
  };

void sendRF_OFF(){
  //RF_init();
  //Pick socket to control 
  signal.parts.unit = 10; 
  signal.parts.status = PROOVE_SIGNAL_OFF;
  prooveSignalSendData(transmissionPin, signal.data);
  };
*/
 
