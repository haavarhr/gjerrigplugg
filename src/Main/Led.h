#ifndef MY_LED_H
#define MY_LED_H

#include <Arduino.h> // OBS:  This include is necessary in header files to use the specific Arduino functions and types (think of pinMode(), digitalWrite(), byte)

class Led {
  
  private:
    byte pin;
    
  public:
    Led(byte pin);
    void init();
    void on();
    void off();
};
#endif
