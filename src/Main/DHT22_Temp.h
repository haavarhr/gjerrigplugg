#ifndef MY_TEMP_H
#define MY_TEMP_H

#include <Arduino.h>
#include <DHT.h>    //DHT sensor library by Adafruit, for temp/moist sensor
#include <DHT_U.h>

void initTempSensor();
float readTemperature();


#endif
