#include "DHT22_Temp.h"

#define DHTPIN 13      // what pin we're connected to, // D7 = GPIO13
#define DHTTYPE DHT22   // DHT 22  (AM2302)
DHT dht(DHTPIN, DHTTYPE);



void initTempSensor(){
  
  dht.begin();
}


float readTemperature(){

  // Wait a few seconds between measurements.
  delay(2000);
  
  // Reading temperature or humidity takes about 250 milliseconds!
  // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
  // Read temperature as Celsius
  float t = dht.readTemperature();
  //float h = dht.readHumidity();

   // Check if any reads failed and exit early (to try again).
  if (isnan(t)) { //isnan(h) ||
    Serial.println("Failed to read from DHT sensor!");
    Serial.println("Retrying in 2 seconds...");
    t = readTemperature(); // Recursion ooouuuuooohhh pass på!
    //return;
  }

//  Serial.print("Temperature: "); 
//  Serial.print(t);
//  Serial.println(" *C ");
  
  return t;
}



 
