#ifndef MY_RF_H
#define MY_RF_H

#include <Arduino.h>
#include <ProoveSignal.h>

//extern "C" {
  //#include <ProoveSignal.h>
//}



// RF-modul, bygger på: https://github.com/tirithen/Arduino-ProoveSignal
//Krever installasjon av eksternt bibliotek via arduino IDE'en: Sketch--> include library --> add.zip library --> velg arduino-ProoveSignal.zip som ligger i samme mappe som main

//Manual for Proove-stikkontakten: https://oldweb.telldus.net/wp-content/uploads/TSP200_Manual.pdf
// Program remote code: Hold programming button for 3 sec --> led starts slowly blinking --> press on button on remote within 10 seconds --> led blinks twice as confirmation

// RadioFreqency: http://tech.jolowe.se/home-automation-rf-protocols/
// Sample RF signal: https://rayshobby.net/interface-with-remote-power-sockets-final-version/


class RFtransmitter {
  private:
    ProoveSignal signal;
    char transmissionPin;
    
  public:
    RFtransmitter(char transmissionPin);
    void init();
    void socket(byte powerState);
    //void socketOFF();
};






//-------- OLD ----------

/*
// TODO: argument for remote ID,transmissionPin, og signal.parts.unit
void RF_init();
void sendRF_ON();
void sendRF_OFF();
*/

#endif
