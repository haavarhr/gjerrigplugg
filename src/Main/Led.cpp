#include "Led.h"

Led::Led(byte pin) {
  // Use 'this->' to make the difference between the
  // 'pin' attribute of the class and the 
  // local variable 'pin' created from the parameter.
  this->pin = pin;
  init();
}

void Led::init() {
  pinMode(pin, OUTPUT);
  off();
}

void Led::on() {
  // Håndterer at LED_BUILTIN er active low, alle andre pins er active high
  if (pin == LED_BUILTIN){
      digitalWrite(pin, LOW);
    }
  else{
    digitalWrite(pin, HIGH);
    }
}

void Led::off() {
  if (pin == LED_BUILTIN){
    digitalWrite(pin, HIGH);
  }
  else {
    digitalWrite(pin, LOW);
    }
}
