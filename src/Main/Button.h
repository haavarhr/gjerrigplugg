#ifndef MY_BUTTON_H
#define MY_BUTTON_H

#include <Arduino.h>

class Button {
  
  private:
    byte pin;
    byte state;
    bool toggleState; //test toggle button
    byte lastReading;
    unsigned long lastDebounceTime = 0;
    unsigned long debounceDelay = 75; //Old:50  Tweak denne litt. litt ustabil, kan vere pga breadboard.
    
    
  public:
    Button(byte pin);
    void init();
    void update();
    byte getState();
    byte getToggleState();
    bool isPressed();
    bool isToggled();
};
#endif
