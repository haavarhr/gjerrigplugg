#ifndef MY_PI_REG_H
#define MY_PI_REG_H

#include <Arduino.h> // OBS:  This include is necessary in header files to use the specific Arduino functions and types (think of pinMode(), digitalWrite(), byte)
#include #include "RF.h"



int piRegulator();

/*
class Led {
  
  private:
    byte pin;
    
  public:
    Led(byte pin);
    void init();
    void on();
    void off();
};
*/

#endif
