#include <math.h>
#define temperatureSensor A0 //pinnen temeratursensoren er paa

void setup() {
  pinMode(temperatureSensor, INPUT);
  Serial.begin(9600);
}

//returnerer temperaturen i celcius
double temperature(){
  double V_disctrete=analogRead(temperatureSensor);
  //double v=3.3*V_disctrete/1024;
  //Serial.println(V_disctrete);
  //Serial.println(v);
  double tmp=-log(1024/V_disctrete - 1);
  tmp= tmp/3977 + 1/298.15;
  return 1/tmp -273.15;
}
void loop() {
  Serial.println(temperature());
  delay(1000);
}
