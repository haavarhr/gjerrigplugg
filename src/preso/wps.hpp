#ifndef WPS_H
#define WPS_H

#include <ESP8266WiFi.h>
#include <Arduino.h>


// Constants

const int pinWps = 12;          // WPS pin number.
const int wpsStartDelay = 1e2;  // Time to wait from button press to WPS start, in milliseconds.
const int wpsDelay = 2e4;       // Time from pressing WPS button until WPS times out.

// Functions
bool conWPS();
ICACHE_RAM_ATTR void btnWpsPress();

#endif
