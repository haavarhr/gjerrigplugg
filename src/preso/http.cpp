#include "http.hpp"


ESP8266WebServer server(80); // Create a webserver object that listens for HTTP request on port 80
float currentPrice = 0;


// Web Server address.
const char *host = "http://rest.fjordkraft.no/pricecalculator/priceareainfo/private/7030";

void httpGetData(){
  
  // Run HTTP request and apply relay logic below.
  HTTPClient http;    //Declare object of class HTTPClient

  Serial.print("Request Link:");
  Serial.println(host);

  http.begin(host);     //Specify request destination

  int httpCode = http.GET();            //Send the request
  String payload = http.getString();    //Get the response payload from server

  if (httpCode == 200) {
    handle_json(&payload);
  } else {
    Serial.println("Error in response");
  }

  http.end();  //Close connection
}

void handle_json(String *s) {
  if (s == NULL)
    return;

  const size_t capacity = 1024; //JSON_OBJECT_SIZE(3) + JSON_ARRAY_SIZE(2) + 60;
  DynamicJsonDocument doc(capacity);

  // Parse JSON object
  DeserializationError error = deserializeJson(doc, *s);

  // Test if parsing succeeds.
  if (error) {
    Serial.print(F("deserializeJson() failed: "));
    Serial.println(error.f_str());
    return;
  }
  // Format:
  // {"countyNo":16,"postalLocation":"TRONDHEIM","price":33.0687,"priceAreaId":3,"priceAreaName":"Midt Norge"}
  // Decode JSON/Extract values
  currentPrice = doc["price"];
  Serial.print("Price: ");
  Serial.println(currentPrice);
}

void setupWebServer(){
  pinMode(led, OUTPUT);

  
  
  if (MDNS.begin("esp8266")) {              // Start the mDNS responder for esp8266.local
    Serial.println("mDNS responder started");
  } else {
    Serial.println("Error setting up MDNS responder!");
  }

  server.on("/", HTTP_GET, handleRoot);     // Call the 'handleRoot' function when a client requests URI "/"
  server.on("/LED", HTTP_POST, handleLED);  // Call the 'handleLED' function when a POST request is made to URI "/LED"
  server.onNotFound(handleNotFound);        // When a client requests an unknown URI (i.e. something other than "/"), call function "handleNotFound"

  server.begin();                           // Actually start the server
  Serial.println("HTTP server started");
}

void handleRoot() {                         // When URI / is requested, send a web page with a button to toggle the LED
  server.send(200, "text/html", "<meta charset=\"UTF-8\"><h1>Dagens strømpris er: " + String(currentPrice) + "<br/></h1><form action=\"/LED\" method=\"POST\"><input type=\"submit\" value=\"Toggle LED\"></form>");
}

void handleLED() {                          // If a POST request is made to URI /LED
  digitalWrite(led,!digitalRead(led));      // Change the state of the LED
  server.sendHeader("Location","/");        // Add a header to respond with a new location for the browser to go to the home page again
  server.send(303);                         // Send it back to the browser with an HTTP status 303 (See Other) to redirect
}

void handleNotFound(){
  server.send(404, "text/plain", "404: Not found"); // Send HTTP status 404 (Not Found) when there's no handler for the URI in the request
}

void handleMyClient(){
  server.handleClient();
}
