#include "wps.hpp"
#include "preso.hpp"
#include "http.hpp"

// Used for WiFi busy waiting.
int counter = 0;
bool b = true;

void setup() {
  Serial.begin(115200);
  delay(10);
  Serial.println("Gjerrigplugg");
  startMillis = millis();
  if(WiFi.SSID().length() > 0){ 
    Serial.print("Connecting to network: ");
    Serial.println(WiFi.SSID());
    WiFi.disconnect(true);
    WiFi.mode(WIFI_STA);
    WiFi.begin(WiFi.SSID());
    while (WiFi.status() != WL_CONNECTED) {
      delay(500);
      Serial.print(".");
    }
    Serial.println("");
    Serial.println("WiFi connected");
    Serial.println("IP address set: ");
    Serial.println(WiFi.localIP()); // Print LAN IP
  }
  attachInterrupt(digitalPinToInterrupt(pinWps), btnWpsPress, FALLING);
  
}

void loop() {
  if (WiFi.status() == WL_CONNECTED) { //if we are connected to eduroam network
    counter = 0; //reset counter
  } else if (WiFi.status() != WL_CONNECTED && WiFi.SSID().length()) { //if we lost connection, retry
    WiFi.begin(WiFi.SSID());
  }
  while (WiFi.status() != WL_CONNECTED) { //during lost connection, print dots
    delay(2e3);
    Serial.print(".");
  }

  if(b){
    setupWebServer();
    b = false;
  }
  handleMyClient();
  currentMillis = millis();  //get the current time
  if (currentMillis - startMillis >= pollInterval)  //test whether the period has elapsed
  {
    httpGetData();
    startMillis = currentMillis;  //IMPORTANT to save the start time of the current LED brightness
  }

}
