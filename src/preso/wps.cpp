#include "wps.hpp"

// Interrupt Service Routine for WPS button.
ICACHE_RAM_ATTR void btnWpsPress() {
  // Run WPS routine.
  Serial.println("Pressed WPS button");
  digitalWrite(LED_BUILTIN, LOW);
  bool wpsSuccess = conWPS();
  Serial.println("WPS DONE!");
  digitalWrite(LED_BUILTIN, HIGH);
}

// Aquire WiFi SSID and PSK by using Wireless protected Setup (WPS).
// Returns true if WiFi connection was seuccessfully established using the WPS protocol,
// false if not.
bool conWPS() {
  Serial.println("WPS config start");
  // WPS works in STA (Station mode) only -> not working in WIFI_AP_STA !!! '
  
  if(!WiFi.mode(WIFI_STA)){
    Serial.println("Configuring ESP8266 NIC to station mode.\n");
    WiFi.mode(WIFI_STA);
  }

  // Disconnect ESP8266 from the currently connected wifi network.
  if(WiFi.status() == WL_CONNECTED){
    Serial.printf("Disconnecting from current network '%s'\n", WiFi.SSID().c_str());
    WiFi.disconnect();
  }

  // Delay program execution by 'wpsDelay' seconds to allow user to press WPS button on 
  // WiFi router/access point.
  Serial.println("Press the WPS button on your WiFi router now.");
  // Begin timing the delay before beginning WPS.
  int begin = millis();
  while(millis() - begin < wpsStartDelay)
    continue;
  
  bool wpsSuccess = WiFi.beginWPSConfig();

  Serial.printf("Begin WPS wait time. (%i seconds)\n", wpsDelay / 1000);
  begin = millis();
  while(millis() - begin < wpsDelay && WiFi.status() != WL_CONNECTED){
    ESP.wdtFeed();
    if(WiFi.SSID().length() > 0)
      WiFi.begin(WiFi.SSID());
  }
  
  Serial.println("End WPS wait time.");
  if(wpsSuccess) {
      //WiFi.begin(WiFi.SSID());
      // Check for empty SSID, in case of a timeout.
      if(WiFi.SSID().length() > 0) {
        
        // WPSConfig has connected in STA mode successfully to the new station. 
        if(WiFi.status() == WL_CONNECTED)
          Serial.printf("WPS finished. Connected successfully to SSID '%s'\n", WiFi.SSID().c_str());
        else
          Serial.printf("WPS attempted to connect to network with SSID '%s', but failed.\n", WiFi.SSID().c_str());
          
      } else {
        wpsSuccess = false;
        Serial.println("WPS timed out...");
      }
  }
  return wpsSuccess; 
}
