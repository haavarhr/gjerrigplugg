#ifndef PRESO_H
#define PRESO_H

// Data polling interval in hours. Leave (36e5) untouched. Adjust the left factor to adjust hours..
const long pollInterval = 6e4; //36e5;

// Time to wait before rerying connection if no WiFi connection is present.
const int noNetRetryDelay = 1e3;
unsigned long startMillis;
unsigned long currentMillis;
#endif
