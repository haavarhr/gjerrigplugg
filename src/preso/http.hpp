#ifndef HTTP_H
#define HTTP_H

#include <ArduinoJson.h>
#include <ESP8266HTTPClient.h>
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WiFiMulti.h> 
#include <ESP8266mDNS.h>
#include <ESP8266WebServer.h>

 


// Constants
const int led = 16;

// Functions

void httpGetData();
void handle_json(String *s);
void setupWebServer();
void handleRoot();              // function prototypes for HTTP handlers
void handleLED();
void handleNotFound();
void handleMyClient();
#endif
