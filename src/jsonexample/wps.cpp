#include "wps.h"

char *wifiName, *wifiPass;
const int wpsDelay = 3e3;

// Aquire WiFi SSID and PSK by using Wireless protected Setup (WPS).
// Returns true if WiFi connection was seuccessfully established using the WPS protocol,
// false if not.
bool conWPS() {
  Serial.println("WPS config start");
  // WPS works in STA (Station mode) only -> not working in WIFI_AP_STA !!! '
  
  if(!WiFi.mode(WIFI_STA)){
    Serial.println("Configuring ESP8266 NIC to station mode.\n");
    WiFi.mode(WIFI_STA);
  }

  // Disconnect ESP8266 from the currently connected wifi network.
  if(WiFi.status() == WL_CONNECTED){
    Serial.printf("Disconnecting from current network '%s'\n", WiFi.SSID().c_str());
    WiFi.disconnect();
  }

  // Delay program execution by 'wpsDelay' seconds to allow user to press WPS button on 
  // WiFi router/access point.
  Serial.println("Press the WPS button on your WiFi router now");
  // Begin timing the delay before beginning WPS.
  int begin = millis();
  while(millis() - begin < wpsDelay)
    continue;

  Serial.println("\nEND WPS DELAY!\n");
  
  bool wpsSuccess = WiFi.beginWPSConfig();
  
  while (WiFi.status() == WL_DISCONNECTED) {
    //delay(500);
    Serial.print(".");
  }
  Serial.print("\n");
  
  if(wpsSuccess) {
      // Check for empty SSID, in case of a timeout.
      String newSSID = WiFi.SSID();
      if(newSSID.length() > 0) {
        
        // Free heap memory of old SSID and password.
        if(wifiName != NULL)
          free(wifiName);
        
        if(wifiPass != NULL)
          free(wifiPass);

        // Allocate memory on heap for new SSID and PSK.
        wifiName = (char *) malloc(newSSID.length());
        wifiPass = (char *) malloc(WiFi.psk().length());

        // Copy SSID and PSK from WPS into global variables, for later use, if needed.
        strcpy(wifiName, newSSID.c_str());
        strcpy(wifiPass, WiFi.psk().c_str());
        
        // WPSConfig has connected in STA mode successfully to the new station. 
        Serial.printf("WPS finished. Connected successfull to SSID '%s'", wifiName);

        // TODO: Slett debug?
        // DEBUG:
        Serial.print("Connected:");
        Serial.printf(WiFi.status() == WL_CONNECTED ? "true" : "false");
      } else {
        wpsSuccess = false;
      }
  }
  return wpsSuccess; 
}
