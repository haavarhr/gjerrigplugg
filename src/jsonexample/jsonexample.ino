#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <ArduinoJson.h>
#include "wps.h"

using namespace tinyxml2;

// Web Server address.
const char *host = "http://rest.fjordkraft.no/pricecalculator/priceareainfo/private/7030";

// WPS pin number.
const int pinWps = 4;

// Data polling interval in hours. Leave (36e5) untouched. Adjust the left factor to adjust hours..
const long pollInterval = 3e4; //1 * (36e5);

// Time to wait before rerying connection if no WiFi connection is present.
const int noNetRetryDelay = 3e3;


// Interrupt Service Routine for WPS button.
ICACHE_RAM_ATTR void btnWpsPress() {
  // Run WPS routine.
  Serial.println("Pressed WPS button");
  digitalWrite(LED_BUILTIN, LOW);
  bool wpsSuccess = conWPS();
  Serial.println("WPS DONE!");
  digitalWrite(LED_BUILTIN, HIGH);
}

void handle_json(String *s) {
  if (s == NULL)
    return;

  const size_t capacity = 1024; //JSON_OBJECT_SIZE(3) + JSON_ARRAY_SIZE(2) + 60;
  DynamicJsonDocument doc(capacity);

  // Parse JSON object
  DeserializationError error = deserializeJson(doc, *s);

  // Test if parsing succeeds.
  if (error) {
    Serial.print(F("deserializeJson() failed: "));
    Serial.println(error.f_str());
    return;
  }
  // Format:
  // {"countyNo":16,"postalLocation":"TRONDHEIM","price":33.0687,"priceAreaId":3,"priceAreaName":"Midt Norge"}
  // Decode JSON/Extract values
  float price = doc["price"];
  Serial.print("Price: ");
  Serial.println(price);
}

void setup() {

  Serial.begin(115200);
  delay(10);
  Serial.println();

  Serial.print("Configuring WPS pin [");
  Serial.print(pinWps);
  Serial.println("] as INPUT");

  // Set pinWps as an input pin, and attach an ISR-function when clicked.
  pinMode(pinWps, INPUT);
  attachInterrupt(digitalPinToInterrupt(pinWps), btnWpsPress, FALLING);

  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW);

  WiFi.mode(WIFI_STA);

  if (WiFi.status() == WL_DISCONNECTED) {
    WiFi.begin();

    while (WiFi.status() != WL_CONNECTED) {
      delay(500);
      Serial.print(".");
    }

    Serial.println("WiFi connected");
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());
  }
}

void loop() {
  if (WiFi.status() == WL_CONNECTED) {
    HTTPClient http;    //Declare object of class HTTPClient

    Serial.print("Request Link:");
    Serial.println(host);

    http.begin(host);     //Specify request destination

    int httpCode = http.GET();            //Send the request
    String payload = http.getString();    //Get the response payload from server

    if (httpCode == 200) {
      handle_json(&payload);
    } else {
      Serial.println("Error in response");
    }

    http.end();  //Close connection

    delay(pollInterval);  // GET Data at every pollInterval hours
  } else {
    // No WiFi, retry connection after delay.
    Serial.println("No WiFi!");
    delay(noNetRetryDelay);
  }
}
