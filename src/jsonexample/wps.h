#ifndef gj_wps_h
#define gj_wps_h

#include <ESP8266WiFi.h>
#include <Arduino.h>

// TODO: Vurder å fjern disse, og sjekk opp mot WiFi.SSID() og WiFi.psk() manuelt.
// WiFi parameters.
extern char* wifiName;
extern char* wifiPass;

#define maxLengthSSID 63
#define maxLengthPSK 32

// EEPROM addresses.
#define eepromSSID 0
#define eepromPSK 64

bool conWPS();

#endif
