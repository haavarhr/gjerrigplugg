#include "DHT.h"

#define DHTPIN D7     // sensor pin
#define DHTTYPE DHT22   // DHT 22  (AM2302)
#define ledPin D1

DHT dht(DHTPIN, DHTTYPE);

void setup() {
  Serial.begin(9600); 
  dht.begin();
  pinMode(ledPin, OUTPUT);
}
int telling = 0;
float oldTemp=0;
float oldHum=0;

void loop() {
  delay(5000);

  float temperature = dht.readTemperature(); //i celcuis
  float humidity = dht.readHumidity();
  Serial.print("measurement ");
  Serial.print(telling);
  Serial.print(": t=");
  Serial.print(temperature);
  Serial.print("C; h=");
  Serial.print(humidity);
  Serial.println("%;");
  telling++;

  //setter ledden på hvis temperaturen går ned, og av hvis temperaturen går opp
  if (oldTemp>temperature){
    digitalWrite(ledPin, 1);
  }
  else if(oldTemp<temperature){
    digitalWrite(ledPin, 0);
  }
  oldTemp=temperature;
  oldHum=humidity;
}
