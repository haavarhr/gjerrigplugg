# Gjerrigplugg
TFE4852 - IoT gruppe 4

## Gruppemedlemmer
- Halvor Johan Steine
- Håvard Hammer Ramberg
- Håvard Melheim
- Jostein Brovold
- Kristoffer Fossmo
- Magnus Westbye Ølstad

## Installasjon
1. Installer Arduino IDE: https://www.arduino.cc/en/software 
2. Følg denne Guiden:https://create.arduino.cc/projecthub/electropeak/getting-started-w-nodemcu-esp8266-on-arduino-ide-28184f 
3. Velg Board: NodeMCU 1.0
4. Kun på Mac: Installer driver for USB-chippen dersom esp'en ikkje dukke opp i Arduino IDE'en

## Avhengigheter
1. **ProoveSignal bibliotek for RF-sending**.  
Krever installasjon via arduino IDE'en: Sketch--> include library --> add.zip library --> velg arduino-ProoveSignal.zip som ligger i mappa: gjerrigplugg\src\Main
2.
3.


## Hardware
1. NodeMCU Devkit V1.3 (ESP8266) 
2. 433MHz RF-sender https://www.aliexpress.com/item/32298304710.html?spm=a2g0s.9042311.0.0.3dce4c4dusz6uV
3. Proove/Anslut stikkontakt: https://oldweb.telldus.net/wp-content/uploads/TSP200_Manual.pdf
